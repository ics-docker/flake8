FROM registry.esss.lu.se/ics-docker/centos-miniconda3:4.4


ENV FLAKE8_VERSION 3.5.0

# Install flake8 both in Python 3.6 and Python 2.7 environments
RUN conda create -y -n python36 python=3.6 flake8=$FLAKE8_VERSION \
  && conda create -y -n python27 python=2.7 flake8=$FLAKE8_VERSION \
  && conda clean -tipsy \
  && mkdir -p /home/conda/.config

# Add Flake8 global user configuration
COPY flake8 /home/conda/.config/
