flake8 docker image
===================

Docker_ image to run Flake8_.

    It is very important to install Flake8 on the correct version of Python for your needs.
    If you want Flake8 to properly parse new language features in Python 3.5 (for example),
    you need it to be installed on 3.5 for Flake8 to understand those features.
    In many ways, Flake8 is tied to the version of Python on which it runs.

This is why flake8 is installed in two different environments.

To use flake8 on Python 3.6 code, run::

    $ /opt/conda/envs/python36/bin/flake8


To use flake8 on Python 2.7 code, run::

    $ /opt/conda/envs/python27/bin/flake8


flake8 is not put in the default PATH to make explicit on which Python version you run it.

A global user configuration is installed to ignore E501 (line too long) errors by default.


.. _Docker: https://www.docker.com
.. _Flake8: http://flake8.pycqa.org/en/latest/index.html/
